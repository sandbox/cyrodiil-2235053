
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage


INTRODUCTION
------------

Developer: Raphael Duerst <raphael.duerst@innoveto.com>

The Mail verification modules asks the user at his first login (or the next login
after the module was enabled) if his mail address is still correct.
This is very useful if the users on a website are imported and didn't register
themselves.


INSTALLATION
------------

1. Copy this mail_verification/ directory to your sites/SITENAME/modules
   directory.

2. Enable the module in admin/modules


USAGE
-----

After the module has been enabled, every user will be asked at his next login
if his mail address is still correct.

The mail field is automatically filled out with the old mail address and the
user can confirm this address or type in a new one and save it.

If the user clicks on 'Ask later' instead, he will be redirected to the front
page and automatically asked at his next login.
